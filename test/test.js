login=new LoginController();
drawboard=new DrawBoardController();

//data=["apples","bananas","pears","mangos"]
function LoginController(){
    var self = this;
    self.username = "";
    self.password = "";
    self.secret = "";
    self.hash = "";
    self.json = "";
    self.fruits=["apples 3.59 Eur/kg", "bananas 1.25 Eur/kg", "pears 8.84 EUR/kg", "mangos 9.23 Eur/kg"]
    self.selectedFruit=1;
    //self.newFruit="prunes 8.99£/lb.";
    self.addFruit = _addFruit;
    self.send = _send;
    function _send($hash){
        console.log(this,self,login,$hash);
    }
    function _addFruit($text){
        self.fruits.push($text);
        self.newFruit = "";
        return self.newFruit;
    }
}
function DrawBoardController(){
    var self=this;
    self.canvas=null;
    self.test=function(canvas){console.log(self,canvas,canvas.getContext('2d'));}
}

run
.register({
    run:function(){
        login.hash=btoa([login.username,login.password,login.secret].join(":"));
    },
    runonce:false
})
.register({
    run:function(){
        login.json=JSON.stringify({username:login.username,password:login.password,secret:login.secret,selected:login.selectedFruit,list:login.data},null,2);
    },
    runonce:false
})
.wire(
    (function(){
        var httpQueue=new HttpQueue({
            method:"POST",
            url:"/backend/test.php",
            ready:function(_transaction){
                console.log("SEND-OK",_transaction);
            },
            error:function(_transaction){
                console.log("SEND-ERROR",_transaction);
            },
            finished:function(d){
                console.log("SEND-idling",d)
            },
            progress:function(d){
                console.log("SEND-progress",d);
            }
        })
        console.log(httpQueue);

        var lhash="";
        var hashFn=function(a){ return btoa(JSON.stringify(a)) }
        var hashSend=_.throttle(function (h){
            var _h=hashFn(h)
            if(_h!==lhash){
                lhash=_h;
                httpQueue.queue(h);
            }
        },10)
        return [
            function(){
                var $ret=[login.username,login.password,login.secret]
                return $ret;
            },
            function($username,$password,$secret){
                var $hash=btoa([$username,$password,$secret].join(":"));
                return [{
                    username : login.username,
                    password : login.password,
                    secret : login.secret,
                    hash : $hash
                }];
            },
            function(hash){
                hashSend(hash);
            }
        ]
    })()
);
setTimeout(function(){
    ["GET","POST","HEAD","DELETE","OPTIONS","CONNECT","PATCH","PUT","TRACE"]
    .forEach(function(method,index){
        console.log(method)
        try{
            $http({
                method:method,
                url:"/backend/store.php",
                data:{
                    requestMethod:method,
                    ord:index
                },
                ready:function(r){console.log("TEST",r)},
                error:function(r){console.log("TEST-error",r)}
            });
        }catch(error){
            console.log(method+" not allowed");
        }
    });
},1000);

setTimeout(function(){
    $http({
        method:"POST",
        url:"/backend/auth-basic.php",
        data:{
            requestMethod:method,
            ord:index
        },
        ready:function(r){console.log("TEST",r)},
        error:function(r){console.log("TEST-error",r)}
    });
},2000)