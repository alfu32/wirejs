# The `wire-engine` Project
The **engine** aims to be a fast and very simple synchronization engine for the DOM, letting the developer **declare** transports of the value of a view property or attribute to a controller field and viceversa.

###### _Let's take a look at the example below :_

We are setting up **bidirectional** wires:
- between `UserNameInput.value` and `Controller.user.name`
- between `UserRoleSelect.value` and `Controller.eyes.type`

The left and right will be kept in sync giving priority to `Controller` to `View` unless the view has user focus (then `View` to `Controller` has priority)


Then we're setting up **unidirectional** wires: 
- from `Controller.user.name` to `UserView.innerHTML`
- from `Controller.eyes.color` to `UserView.style.backgroundColor`

Now we have a problem : _synchronizing ctrl.eyes.color wirth ctrl.eyes.type_. We have to transform `type` to `color`.

We'll be doing that using a wire (RoleToColorWire) :
- it reads `ctrl.eyes.type`
- transforms it into a color
- writes the color to `ctrl.eyes.color`

<sub>The wiring mechanics is actually very similar to IBuilder in XCode</sub>

```mermaid
graph LR;
subgraph Controller
    subgraph UserController
        ctrl.user
        ctrl.eyes.color
        ctrl.eyes.type
    end
end
subgraph View
    subgraph UserView
        uv.backgroundColor---|<<<<<|ctrl.eyes.color
        uv.innerHTML---|<<<<<|ctrl.user
    end
    subgraph UserNameInput
        uni.value---|<<<::>>>|ctrl.user
    end
    subgraph UserRoleSelect
        us.value---|<<<::>>>|ctrl.eyes.type
    end
end
subgraph wires
    subgraph RoleToColorWire
        RD-ctrl.eyes.type-->TFORM-type-to-color
        TFORM-type-to-color-->WR-ctrl.eyes.color
        ctrl.eyes.type-->|pull|RD-ctrl.eyes.type
        WR-ctrl.eyes.color-->|push|ctrl.eyes.color
    end
end
```


The engine aims primarily to facilitate an easy way of :
- **wiring** controller/model and view;
- use a **templating laguage** for your view definitions;
- use controller **methods** to handle _view events_;
- let the developer declare **transformation chains** or **wires** where an initial _information packet_(IP) can be transformed and copied to 0...N memory locations (controllers).

It is a framework as unopionated as it can possibly be, imposing only few constraints.
It is ment to be used on a **navigator** platform but wires can be also used on _nodejs_. 


# Installation
    `git clone https://gitlab.com/alfu32/wirejs.git`
    ( mind the submodule updates )
## Quick Start
    clone the repo, copy the files from `./test/`, copy `engine.js`, fix the references and youre good to go.

# Built-in directives
## `wire` and  `controller`

The `wire` directive instructs the machine to keep view and controller fields/property/accessors in sync.

DOM Wiring works in conjunction with `controller`: 
    at runtime the engine will look for the first controller reference up the DOM tree and make all controller references relative to the found controller reference.

While wire declarations are relative to controller, controller references are absolute (relative to window, top or whichever top/root object it might find). If no controller directive is found youl be accessing the global/top/root object

The synchronization direction/operator tells the machine from which to which accessor to propagate values and _how_ to do it (from model to view, from view to model or both --- see **operators**).

### syntax

`controller`=`"<absolute.path.to.data.structure>"`

`wire`=`"[nodeAccessor]<operator><controllerAccessor>,..."`

`nodeAccessor` is an accessor at the node(view) level : value, innerHTML, style.backgroundColor etc... are all valid expressions. The node accessor expression is a shortcut for fully expanded exression this.value, this.innerHTML, etc...

`operator` is one of the symbols `:>`,`<:` or `::`

`controllerAccessor` specifies an access path within the controller scope. this works in conjunction with the `controller` directive that indicates the absolute path to the controller.

void left hand references will be tresolved to `this`, where `this` is the node.

void right hand references will evaluate to `controller` or `window[""]`

### operators
- unidirectional `:>` : instructs the engine to pull the accessor value from the source and feed it into the target
- unidirectional `<:` : instructs the engine to pull the accessor value from the target and feed it into the source
- omnidirectional `::` : instructs the engine to keep the source and the target synchronized. It is equvalent to both the above operators giving preference to model to view updates when the view has no focus and to view to model updates when the view has user focus.

### example
```html
<form controller="someController" wire=":>myForm">
    <!-- omnidirectional synchronization -->
    <input wire="value::someProperty"/>
    <!-- unidirectional synchronization -->
    <input wire="innerHTML<:someProperty"/>
    <!-- wiring up properties of the same view -->
    <input type="color" wire="value:>$$.backgroundColor,style.backgroundColor<:$$.backgroundColor"/>
</form>
```

### notes

- bidirectional synchronization is a complicated problem to solve, _the solution provided by map is to block controller to view updates if the user has focus_ on that particular UI element.
- method invocations are modifing data in most cases, that's why invokation is given a higher priority than view to model updates.
- if the target property does not exist in the controller scope, it will be created 
- TODO__002 :: see how to propagate information packets (IP) locks


## `on`
`on` directive declares an event listener on the dom node. 

### syntax
` on="<eventName>:controller.method.invocation.reference([[[optional],argument],list])" `

```html
    <button on="click:someController.someMethod()">do something</button>
    <button on="click:someController.someMethod(someController.someField)">do something else</button>
    <button on="click:someController.someMethod(someController.someMethod())">do yet another thing</button>
```


## `engine`
specifies that the subtree is a view definition and the engine that is used to generate the actual view.
the synch-engine does a view update using diff-dom, 
The examples are using handlebars engine. but you can use any engine that you might choose, and you can use multiple engines in the same app (screen)


### Example
View
```hbs
    <h1>Users List</h1>
    <ul engine="Handlebars" controller="someOtherController">
    {{#each users}}
        <li>{{name}}</li>
    {{/each}}
    </ul>
    
    <ul engine="Handlebars" controller="someOtherController">
    {{#each users}}
        <li>
            <input wire="value::users[{{@index}}].name" value="{{name}}"/>
            <button on="click:deleteUser({{@index}})">DEL</button>
        </li>
    {{/each}}
        <li>
            <input wire="value::newUser.name" value="{{name}}"/>
            <button on="click:addNewUser()">ADD</button>
        </li>
    </ul>
```
Controller
```js
    var someOtherController = new SomeOtherController();

    function SomeOtherController(){
        var self=this;
        self.users=[];
        self.newUser={}
        self.addUser=function(){
            self.users.push(self.newUser);
        }
        self.deleteUser=function($index){
            self.users=self.users.filter(function(v,i){ return i!=$index })
        }
    }
```
### notes



## Custom directives (components)
Declaration
```js
    run.component("myDirective", {
        template:"",
        selector:"",
        controller:function(){},
        compile:function(telm,registry,$services){
            return {
                pre:function(){},
                post:function(){}
            }
        }
    })
```
Usage
```html
    <div directive="myDirective" my-directive-param="" controller="someController">
        <input map="innerHTML<:someControllerProperty"/>
        <input map="value:>someControllerProperty"/>
    </div>
```

# Wires

You can declare a pipeline of transformations that will transport and transform data from the first element(s), the source(s) to the last element(s) the destinations; The sources and destinations should be declared as controllers otherwise they will default to undefined.
The transformation happens continuously and is run at every end of an event loop.

```js
run.wire([
    function(){
        return [testController.color,testController.format]
    },
    function(color,format){
    var $return=[null,color,format];
        switch(format){
            case "rgba":
                    $return[0]=COLOR.transform("web","rgba",color)
                break;
        }
        return $return
    },
    function(rgbaColor,color,format){
        testController.rgbaColor = rgbaColor
    }
])
.wire([ function(){return [model.data]},JSON.stringify,function(dataJSON){testController.dataJSON = dataJSON;} ])
.wire([ 
    function(){
        return [testController.dataJSON,messagesController,configController,$services];
    },
    (function (){
        var last="",blockPush=false;
        return function(dataString,messagesController,config,$services){
            if(last !== dataString && blockPush===false){
                $services.saving="Saving model"
                $services.$http({
                    method:config.method,
                    url:config.endpointUrl,
                    headers:{
                        "Authentication-Bearer":config.getAuthToken(),
                        "Content-Type": "application/json, charset=UTF-8"
                    },
                    data:dataString,
                    ready:function(){
                        last=dataString;
                        blockPush=true;
                        $services.saving="Saving model Complete"
                    }
                });
                blockPush=true;
            }
            return dataString;
        }
    })(),
    function(d){
        return testController.dataJSON = d;
     } ])

```
### notes

## Final Notes

### DOM
The engine does no abstraction of DOM everything is as raw as it gets. The reason for this choice is that in 2018, for most APIs, web browsers display the same behaviours and the same access methods. They are standard, one does not need to normalize and abstract that much anymore.

### router / routing
I believe that this provides enough functionality to design any one-screen application, and is perfectly suited to small and medium scale web-apps.

I didn't include a router for the simple reason that web-servers already do that, if your apps is so big that you feel the need to divide is into several pages, then the best way to go is to createseveral pages on the server.
