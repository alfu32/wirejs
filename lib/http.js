$http   =   function $http(params){
    var _voidFunction=function(){}
    var x=new XMLHttpRequest();
    var _diff="";
    var _stream="";
    x.onreadystatechange=function(){
        if(x.readyState==4){
                //console.log(params.url, x.responseText);
            var response;
            try{
                response=JSON.parse(x.responseText);
            }catch(err){
                //try{
                //    var parser = new DOMParser();
                //    response = parser.parseFromString(x.responseText,"text/xml");
                //}catch(err2){
                    response=x.responseText;
                //}
            }
            if(x.status==200){
                (params.ready||_voidFunction)({request:params,headers:x.getAllResponseHeaders(),data:response});
            }else{
                (params.error||_voidFunction)({request:params,headers:x.getAllResponseHeaders(),data:response});
            }
        }else if(x.readyState==3){
            _stream=x.responseText;
            _diff=_stream.substr(_stream.length);
            (params.progress||_voidFunction)({request:params,headers:x.getAllResponseHeaders(),data:response,diff:_diff});
        }
    }
    x.onprogress=function(a,b,c){
        console.log("progress",a,b,c);
    }
    x.open(params.method||"POST",params.url||"localhost",true);
    var payload;
    switch(params.type){
        case "xml":
            x.setRequestHeader("Content-Type","text/xml");
            payload='<?xml version="1.0" encoding="UTF-8"?>'+params.data.children[0].outerHTML
        break;
        case "text":
            x.setRequestHeader("Content-Type","text/plain");
            payload=params.data||null;
        break;
        case "form-data":
            x.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            payload=Object.keys(params.data||[]).map(function(k){return k+"="+params.data[k]}).join("&");
        break;
        default:
            if(params.data){
                x.setRequestHeader("Content-Type","application/json");
                payload=JSON.stringify(params.data)
            }else{
                payload=null;
            }
        break;
    }
    if(params.headers){
        Object.keys(params.headers).forEach(function(k){
            x.setRequestHeader(k,params.headers[k]);
        })
    }
    x.send(payload)
    return {
        abort:function(){x.abort();}
    }
}
