
    String.prototype.wrap=function(lr,r){
        switch(arguments.length){
            case 1:return lr+this+lr;break;
            case 2:return lr+this+r;break;
            default: return this;break;
        }
    }
    String.prototype.tag=function(tag){
        return this.wrap(tag.wrap('<','>'),tag.wrap('</','>'))
    }
    String.prototype.quote=function(s){
        return this.wrap(s.charAt(0),s.charAt(1))
    }