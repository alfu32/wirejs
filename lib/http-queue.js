HttpQueue = (function(){
    return HttpQueue;
    function HttpQueue(params){
        var self=this;
        var $$wait=false;
        var $queue=[];
        var $OnReady=params.ready||function(){};
        var $OnError=params.error||function(){};
        var $OnFinished=params.finished||function(){};
        var $OnProgress=params.progress||function(){};

        self.queue=_queue;

        function _queue(data){
            $queue.unshift(data);
            _run();
            return self;
        }
        function _dequeue(data){
            return $queue.pop();
        }
        function _run(){
            console.log("RUN",$queue);
            if($queue.length===0){
                $$wait=false;
                $OnFinished();return;
            }
            if($$wait)return;
            var _data=_dequeue();
            
            var _params=JSON.parse(JSON.stringify(params));
            _params.data=_data;
            _params.ready=function(_transaction){
                console.log("READY",$queue);
                $OnReady(_transaction)
                $OnProgress({remaining:$queue.length,$queue:$queue});
                $$wait=false;
                _run();
            }
            _params.error=function(_transaction){
                console.log("ERROR",$queue);
                $OnError(_transaction)
                $OnProgress({remaining:$queue.length,$queue:$queue});
                $$wait=false;
                _run();
            }
            $$wait=true;
            $http(_params)
        }
    }


})();
