run = (function(_global){
    var reg=[];
    var $handlers=[];
    var $templates=[];
    var dd = new diffDOM();
    var $$CYCLE="others"
    var OPS={
        BINDING:"bind",
        HANDLER:"on",
        COMPONENT:"component",
        ENGINE:"engine",
        CONTROLLER:"controller",
        APP:"synchro"
    }

    function run(){
        var $$runnables,$CurrentCycle=$$CYCLE;
        switch($$CYCLE){
            case "inject":
                $$runnables = reg.filter(function(r){return r.$cycle==="inject"})
                $$CYCLE="pull";
            break;
            case "pull":
                $$runnables = reg.filter(function(r){return r.$cycle==="pull"})
                $$CYCLE="push";
            break;
            case "push":
                $$runnables = reg.filter(function(r){return r.$cycle==="push"})
                $$CYCLE="others";
            break;
            default:
                $$runnables = reg.filter(function(r){return !r.$cycle})
                $$CYCLE="inject";
            break;
        }
        $$runnables
        .forEach(function(_runnable){
            _runnable.run();
            if(_runnable.runonce)_runnable.hasRun=true;
        })
        setTimeout(run,1);
        reg=reg.filter(function(_runnable){
            return !(_runnable.runonce===true && _runnable.hasRun===true);
        })
    }

    run.first=function(fn){
        reg.push({
            $cycle:"inject",
            runonce:true,
            run:function(){
                console.log($$CYCLE,fn());
            }
        })
    }

    run.last=function(fn){
        reg.push({
            $cycle:"others",
            runonce:true,
            run:function(){
                console.log($$CYCLE,fn());
            }
        })
    }

    run.register=function(_runnable,runonce){
        reg.push(_runnable)
        return run;
    }

    run.wire=function(flow,_CTRL){
        var __CTRL=$CTRL;
        if(typeof(_CTRL)==="function"){
            __CTRL=_CTRL;
        }
        console.log(_CTRL,__CTRL,$CTRL)
        run.register({
            run:/*_.throttle(*/function(){
                flow.reduce(function(ac,fn){
                    var _args=fn.apply(__CTRL(),ac);
                    ac=_.isArray(_args)?_args:[_args];
                    return ac;
                },[])
            }/*,1000)*/,
            runonce:false
        })
        return run;
    }

    run();
    $.ready(function(){
        console.log("ready",$("["+OPS.APP+"]"))
        var __scanBindings={
            run:function(){
                scheduleBindings(document.children[0],$CTRL)
            },
            runonce:true
        }

        run
        .register({
            run:function(){
                scheduleTemplateUpdates(document.children[0],$CTRL)
            },
            runonce:true
        })
        .register(__scanBindings)
        .register({
            run:function(){
                registerHandlers(document.children[0],$CTRL);
            },
            runonce:true
        })
    })

    return run;

    function scanComponent(node){
        $(node).find("["+OPS.COMPONENT+"]")
        .reduce(function(ac,subnode){
            
            var __node = $(subnode);
            var componentName = __node.attr(""+OPS.COMPONENT+"")
            var definition = $components[componentName]
            var Engine = definition.engine
            var source = definition.view()
            var __compile=window[Engine].compile(source);
            var CTRL = new definition.controller();
            var $CTRL = function(_node){ return CTRL; }
            var __template={
                VIEW:subnode,
                compile:__compile,
                update:function(){
                    var $block=$(subnode).find(".block-pull")
                    if(typeof($block.length)!=="undefined" && $block.length>0){
                        return;
                    }
                    $block.removeClass("block-pull")
                    var __currentText = __node.html().replace(/\n|\r|\s{2}|\t/gi,"");
                    var __newText = __compile($CTRL(subnode)).replace(/\n|\r|\s{2}|\t/gi,"");
                    var __newNode = $(subnode).clone();
                    __newNode.html(__newText)
                    var diffD=dd.diff(subnode,__newNode[0])
                    if( __currentText !== __newText
                        && !$block.length ){
                        cleanupHandlers(subnode);
                        cleanupBindings(subnode);
                        dd.apply(subnode, diffD);
                        //$(subnode).html(__newText)
                        //if(node !== subnode )scanTemplate(subnode,$CTRL)
                        scheduleBindings(subnode,$CTRL);
                        registerHandlers(subnode,$CTRL);
                        cleanupOrphanHandlers(subnode);
                        cleanupOrphanBindings(subnode);
                    }
                    $block.addClass("block-pull");
                }
            }
            __node.html("")
            ac.push(__template)
            return ac;
        },[])
    }

    function scanTemplate(node,$CTRL){
        return $(node)
        .find("["+OPS.ENGINE+"]")
        .reduce(function(ac,subnode){
            var __node = $(subnode);
            var Engine = __node.attr(""+OPS.ENGINE+"")
            var source = __node.html()
            var __compile=window[Engine].compile(source);
            var __template={
                VIEW:subnode,
                compile:__compile,
                update:function(){
                    var $block=$(subnode).find(".block-pull")
                    if(typeof($block.length)!=="undefined" && $block.length>0){
                        return;
                    }
                    $block.removeClass("block-pull")
                    var __currentText = __node.html().replace(/\n|\r|\s{2}|\t/gi,"");
                    var __newText = __compile($CTRL(subnode)).replace(/\n|\r|\s{2}|\t/gi,"");
                    var __newNode = $(subnode).clone();
                    __newNode.html(__newText)
                    var diffD=dd.diff(subnode,__newNode[0])
                    if( __currentText !== __newText
                        && !$block.length ){
                        cleanupHandlers(subnode);
                        cleanupBindings(subnode);
                        dd.apply(subnode, diffD);
                        //$(subnode).html(__newText)
                        if(node !== subnode )scanTemplate(subnode,$CTRL)
                        scheduleBindings(subnode,$CTRL);
                        registerHandlers(subnode,$CTRL);
                        cleanupOrphanHandlers(subnode);
                        cleanupOrphanBindings(subnode);
                    }
                    $block.addClass("block-pull");
                }
            }
            __node.html("")
            ac.push(__template)
            return ac;
        },[])

    }
    function refExpression(controllerName,expression){
        if(expression=="this")return controllerName;
        if(expression.charAt(0)==="[")return controllerName+expression;
        else return controllerName+"."+expression;
    }
    function scanBindings(node,$CTRL){
        return $(node)
        .find("["+OPS.BINDING+"]")
        .reduce(function(ac,subnode){
            $(subnode)
            .attr(""+OPS.BINDING+"")
            .split(",")
            .forEach(function(expression){
                var e=expression.match(/(\:\:|\:\>|\<\:)|[a-zA-Z0-9\.\+\-\*\=\(\)\[\]\&\|\;\?\_\%]+/gi)
                if(!e[2] || e[2]===""){
                    if(e[0].match(/(\:\:|\:\>|\<\:)/gi)){
                        e.unshift(e[1]+"")
                    }else{
                        e[2]=e[0]+"";
                    }
                    console.log(e);
                }
                var _push=new Function("CTRL","VIEW","CTRL$lock","\n\
                    var ctrlValue="+refExpression("CTRL",e[2])+";\n\
                    var viewValue="+refExpression("VIEW",e[0])+";\n\
                    if(!CTRL$lock){\n\
                        if(ctrlValue != viewValue){\n\
                            "+refExpression("CTRL",e[2])+"=viewValue;\n\
                        }\n\
                    }\n\
                ")
                var _pull=new Function("CTRL","VIEW","VIEW$lock","\n\
                    var ctrlValue="+refExpression("CTRL",e[2])+";\n\
                    var viewValue="+refExpression("VIEW",e[0])+";\n\
                    if(!VIEW$lock){\n\
                        if(ctrlValue != viewValue){\n\
                            "+refExpression("VIEW",e[0])+"=ctrlValue;\n\
                        }\n\
                    }\n\
                ");
                    $(subnode).on("focus",function(){
                        $(subnode).addClass("block-pull")
                    })
                    $(subnode).on("blur",function(){
                        $(subnode).removeClass("block-pull")
                    })
                    var __pull=_pull
                    _pull = function (CTRL,VIEW){
                        if(!$(subnode).hasClass("block-pull"))__pull(CTRL,VIEW)
                    }

                var $binding={
                    _type : "binding",
                    VIEW : subnode,
                    CTRL : function(){return $CTRL(subnode)},
                    operator : e[1],
                    push : _push,
                    pull : _pull,
                    DEBUG:{
                        expression:expression,
                        e:e,
                        push:_push.toString(),
                        pull:_pull.toString()
                    }
                }
                ac.push($binding)
            })
            return ac;
        },[])
    }

    function scanHandler(node,$CTRL){
        return $(node)
        .find("["+OPS.HANDLER+"]")
        .reduce(function(ac,subnode){
            $(subnode)
            .attr(""+OPS.HANDLER+"")
            .split(",")
            .forEach(function(expression){
                var e=expression.split(/:/gi);
                var _hnd = {
                    _type : "handler",
                    VIEW : subnode,
                    eventName:e[0],
                    eventHandler:e[1],
                    handler:_handler,
                    on:function(){
                        $(subnode).on(e[0], _handler);
                    },
                    off:function(){
                        $(subnode).off(e[0], _handler);
                    }
                }
                ac.push(_hnd);

                var _lpi = e[1].indexOf("("),_fp,_ARGS,_FUN,list;
                if (_lpi > -1) {
                _fp = e[1].substr(_lpi);
                list = _fp.substr(1, _fp.indexOf(")") - 1).split(',').map(function(token){return "CTRL."+token.trim(/\s*/gi)}).join(",")
                _ARGS = new Function("CTRL", "return [" + list + "]");
                _FUN = new Function("CTRL","return CTRL."+e[1].substr(0, _lpi) );
                } else {
                _FUN = new Function("CTRL","return CTRL."+e[1] );
                _ARGS = function() {
                    return [];
                };
                }
                _hnd._ARGS_fn=_ARGS.toString();
                _hnd._FUN_fn=_FUN.toString();
                $(subnode).on(e[0], _handler);
                function _handler(){
                        var _ctrl=$CTRL(subnode);
                        var _args=_ARGS(_ctrl);
                        var _fun=_FUN(_ctrl);
                    run.first(function(){
                        try {
                            _fun.apply(_ctrl, _args );
                        } catch (err) {
                            console.error(err);
                        }
                        return _ctrl;
                    ///   ///   ///   run.last(function(){
                    ///   ///   ///       console.log("@last");
                    ///   ///   ///       return "@last"
                    })
                }
            })
            return ac;
        },[]);
    }

    function $CTRL(subnode){
        var _subnode=subnode||document.documentElement
        var _accessorName = $(_subnode).closest("["+OPS.CONTROLLER+"]").attr(""+OPS.CONTROLLER+"")
        return window[ _accessorName ] || window 
    }

    function cleanupHandlers(node){
        $handlers=$handlers.filter(function(_hnd){
            if(_hnd.VIEW===node)_hnd.off();
            return _hnd.VIEW!==node;
        })
    }
    function cleanupOrphanHandlers(){
        $handlers=$handlers.filter(function(_hnd){
            var $p=_hnd.VIEW;
            while($p!==null && $p!==document){
                $p=$p.parentNode;
            }
            return $p!==null;
        })
    }

    function cleanupBindings(node){
        reg=reg.filter(function(_runner){
            return !(_runner._type=="binding" && _runner.$binding.VIEW===node)
        })
    }
    function cleanupOrphanBindings(node){
        reg=reg.filter(function(_runner){
            if(!_runner.$binding)return true;
            var $p= _runner.$binding.VIEW;
            while($p!==null && $p!==document){
                $p=$p.parentNode;
            }
            return $p!==null;
        })
    }
    function cleanupTemplateUpdates(node){
        reg=reg.filter(function(_runner){
            return !(_runner._type=="template" && _runner.$template.VIEW===node)
        })
    }

    function scheduleBindings(rootNode,$CTRL){

        scanBindings(rootNode,$CTRL)
        .forEach(function($b){
            if($b.operator=='::' || $b.operator==':>' )
            var __runnerPush={
                _type : "binding",
                $cycle:"push",
                $binding:$b,
                run:function(){
                    $b.push($b.CTRL(),$b.VIEW);
                },
                deregister:function(){__runner.runonce=true},
                runonce:false
            }
            if($b.operator=='::' || $b.operator=='<:' )
            var __runnerPull={
                _type : "binding",
                $cycle:"pull",
                $binding:$b,
                run:function(){
                    $b.pull($b.CTRL(),$b.VIEW);
                },
                deregister:function(){__runner.runonce=true},
                runonce:false
            }
            
            if($b.operator=='::' || $b.operator=='<:' )run.register(__runnerPull);
            if($b.operator=='::' || $b.operator==':>' )run.register(__runnerPush);
        })
    }
    function scheduleTemplateUpdates(rootNode,$CTRL){
        
        scanTemplate(rootNode,$CTRL)
        .forEach(function($t){
            run.register({
                _type:"template",
                $cycle:"inject",
                $template:$t,
                run:function(){
                    $t.update();
                },
                runonce:false
            })
        });
    }
    function registerHandlers(rootNode,$CTRL){
        
        scanHandler(rootNode,$CTRL)
        .forEach(function($b){
            $handlers.push($b);
        })
    }
})();
